﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace hw30082020
{
    class Program
    {
        static void Main(string[] args)
        {
            MyLibrary library1 = new MyLibrary();

            library1.AddBook(new Book("Zla", "mamama", "C", "drama") );
            library1.AddBook(new Book("DADA", "mamama", "C", "drama"));
            library1.AddBook(new Book("A", "mamama", "abba", "drama"));
            library1.AddBook(new Book("T", "mamama", "taza", "drama") );

            Console.WriteLine("library1.GetBooksSortedByAuthorName");
            PrintList(library1.GetBooksSortedByAuthorName());
            Console.WriteLine("\nlibrary1.GetBooksTitleSorted");
            PrintList(library1.GetBooksTitleSorted());

            Console.WriteLine("\nGetAuthors");
            var a = library1.GetAuthors();
            PrintList(a);

            Console.WriteLine("\nget first book by author C");
           var b =  library1.GetBookByAuthor("C", true, true);
           Console.WriteLine(b);

           Console.WriteLine("\nget ALL books by author C");
           var c = library1.GetAllBookByAuthor("C", true);
           PrintList(c);

            Console.WriteLine("\nHaveThisBook");
            Console.WriteLine(library1.HaveThisBook("DADA"));
            Console.WriteLine(library1.HaveThisBook("DADAA"));

            Console.WriteLine("");
            Console.WriteLine(library1);

            Console.WriteLine("Get Book DADA");
            Console.WriteLine(library1.GetBook("DADA"));

            Console.WriteLine($"\nRemoveBook(DADA)");
            library1.RemoveBook("DADA");
            Console.WriteLine(library1);


            Console.WriteLine("Get Book DADA after remove");
            var d = library1.GetBook("DADA");
            Console.WriteLine(library1.GetBook("DADA"));

            Console.WriteLine("\n before clear ");
            Console.WriteLine(library1);
            library1.Clear();
            Console.WriteLine("\n after clear");
            Console.WriteLine(library1);
        }

        static void PrintList<T>(List<T> list)
        {
            foreach (T value in list)
            {
                Console.WriteLine(value);
            }
        }

    }
}
