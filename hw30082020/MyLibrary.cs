﻿using System.Collections.Generic;

namespace hw30082020
{
    class MyLibrary
    {
        private Dictionary<string, Book> _mapBooks;
        private Dictionary<string, List<Book>> _mapAuthor ; // etgar itay + etgar azmi 
        
        public MyLibrary()
        {
            _mapBooks = new Dictionary<string, Book>();
            _mapAuthor = new Dictionary<string, List<Book>>();
        }

        public override string ToString()
        {
            string allBooksString = $"we have {_mapBooks.Count} books on our library\n The list:\n";
            int i = 0;
            foreach (string Keybook in _mapBooks.Keys)
            {
                allBooksString += $"{i += 1}. {Keybook}\n";
            }

            return allBooksString;
        }

        
        public bool AddBook(Book book , bool InitializeMoreDictionaries= false)
        {
            if (HaveThisBook(book.Title))
            {
                return false;
            }

            _mapBooks.Add(book.Title,book);
            if (InitializeMoreDictionaries)
            {
                InitializeBookToOtherDictionaries(book);
            }
            return true;
        }



        public bool RemoveBook(string bookTitle)
        {
            if (HaveThisBook(bookTitle))
            {
                _mapBooks.Remove(bookTitle);
                return true;
            }

            return false;
        }

        public bool HaveThisBook(string bookTitle)
        {
            return _mapBooks.ContainsKey(bookTitle);
        }

        public Book GetBook(string bookTitle)
        {
            _mapBooks.TryGetValue(bookTitle, out Book returnBook);
            return returnBook;
        }

        public Book GetBookByAuthor(string authorName , bool etgarSolution = false , bool needTonitializeLists = false)
        {
            if (etgarSolution)
            {
                if (needTonitializeLists)
                {
                    MapBooksToOtherDictionariesInitialize(_mapBooks);
                }

                _mapAuthor.TryGetValue(authorName, out List<Book> outList);
                if (outList == null)
                {
                    return null;
                }
                else
                {
                    return outList[0];
                }

            }
            else
            {
                foreach (Book book in _mapBooks.Values)
                {
                    if (book.Author==authorName)
                    {
                        return book;
                    }
                }

                return null;
            }
        }

        public List<Book> GetAllBookByAuthor(string authorName,  bool needTonitializeLists = false)
        {
            if (needTonitializeLists)
            {
                MapBooksToOtherDictionariesInitialize(_mapBooks);
            }

            _mapAuthor.TryGetValue(authorName, out List<Book> outList);
            if (outList == null)
            {
                return null;
            }
            else
            {
                return outList;
            }
        }

        public void Clear()
        {
            _mapBooks.Clear();
        }

        public List<string> GetAuthors()
        {
            List<string> authorsList = new List<string>();
            foreach (Book book in _mapBooks.Values)
            {
                if (!authorsList.Contains(book.Author))
                {
                    authorsList.Add(book.Author);
                }
            }

            return authorsList;
        }

        public List<Book> GetBooksSortedByAuthorName()
        {
            List<Book> listOfBooks = new List<Book>();
            foreach (Book book in _mapBooks.Values)
            {
                listOfBooks.Add(book);
            }
            listOfBooks.Sort(new BooksSortByAuthorName());
            return listOfBooks;
        }

        public List<Book> GetBooksTitleSorted()
        {
            List<Book> listOfBooks = new List<Book>();
            foreach (Book book in _mapBooks.Values)
            {
                listOfBooks.Add(book);
            }
            listOfBooks.Sort(new BooksSortByTitleName());
            return listOfBooks;
        }

        public int Count()
        {
            return _mapBooks.Count; 
        }


        //etgar atzmi 

        private void MapBooksToOtherDictionariesInitialize(Dictionary<string, Book> mBooks)
        {
            foreach (KeyValuePair<string, Book> valuePair in mBooks)
            {
                InitializeBookToOtherDictionaries(valuePair.Value);
            }
        }
        
        public void InitializeBookToOtherDictionaries(Book book)
        {
            AddBookToListByKeyOfDictionary(book, book.Author, _mapAuthor);
        }
        /// <summary>
        /// A general help function for adding a book by any dictionary key I select into a list
        /// </summary>
        /// <param name="book"></param>
        /// <param name="BookDictionaryKey"></param>
        /// <returns></returns>
        private bool AddBookToListByKeyOfDictionary(Book book, string BookDictionaryKey , Dictionary<string, List<Book>> dictionary)
        {
            if (!DictionaryContainsKey(BookDictionaryKey, dictionary))
            {
                dictionary.Add(BookDictionaryKey,new List<Book>());
            }

            if (DoesTheBookExistInADictionaryByKey(book, BookDictionaryKey,dictionary))
            {
                return false;
            }

            dictionary[BookDictionaryKey].Add(book);
            return true;
        }

        private bool DoesTheBookExistInADictionaryByKey(Book book, string BookDictionaryKey, Dictionary<string, List<Book>> dictionary)
        {
            if (DictionaryContainsKey(BookDictionaryKey, dictionary))
            {
                if (dictionary[BookDictionaryKey].Contains(book))
                {
                    return true;
                }
            }
            return false;
        }

        private static bool DictionaryContainsKey(string BookDictionaryKey, Dictionary<string, List<Book>> dictionary)
        {
            return dictionary.ContainsKey(BookDictionaryKey);
        }


    }
}