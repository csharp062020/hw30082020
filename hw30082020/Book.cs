﻿using System.Collections;
using System.Collections.Generic;

namespace hw30082020
{
    class Book
    {
        public string Title { get; private set; }
        public string Content { get; private set; }
        public string Author { get; private set; }
        public string Category { get; private set; }

        public Book(string title, string content, string author, string category)
        {
            Title = title;
            Content = content;
            Author = author;
            Category = category;
        }

        public override string ToString()
        {
            return $"{nameof(Title)}: {Title}, {nameof(Content)}: {Content}, {nameof(Author)}: {Author}, {nameof(Category)}: {Category}";
        }


        // for etgar azmi 
        public static bool operator ==(Book a, Book b)
        {
            if (a is null && b is null)
            {
                return true;
            }
            else if (a is null || b is null)
            {
                return false;
            }
            return a.Author == b.Author &&
                   a.Title == b.Title &&
                   a.Category == b.Category &&
                   a.Content == b.Content;

        }

        public static bool operator !=(Book a, Book b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {

            return this == obj as Book;
        }
    }

    class BooksSortByAuthorName : IComparer<Book>
    {
        public int Compare(Book x, Book y)
        {
            return x.Author.CompareTo(y.Author); 
        }
    }
    class BooksSortByTitleName : IComparer<Book>
    {
        public int Compare(Book x, Book y)
        {
            return x.Title.CompareTo(y.Title);
        }
    }
}